<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <meta charset="utf-8">

  <title>Vagrant Demo</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css" media="screen">

  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css" media="screen">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.css.map" media="screen">

  <link rel="stylesheet" href="assets/css/theme.css">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="bower_components/html5shiv/dist/html5shiv.js"></script>
    <script src="bower_components/respond/dest/respond.min.js"></script>
  <![endif]-->

  <style type="text/css">
    .success .fa {
      color: #38B538;
    }

    .fail .fa {
      color: #D22020;
    }
  </style>

  <?php require_once "includes/checks.php" ?>
</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <a href="/" class="navbar-brand">Vagrant Demo</a>

      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
  </div>
</div>


<div class="container">
  <div class="page-header" id="banner">
    <div class="row">
      <div class="col-lg-8 col-md-7 col-sm-6">
        <h1>Vagrant Demo</h1>
        <p class="lead">A simple demo to showcase the beauty of vagrant-powered environments.</p>
      </div>
    </div>
  </div>

  <div class="bs-docs-section">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-header">
          <h1 id="typography">Health self-check</h1>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="bs-component">
          <p class="lead success"><i class="fa fa-fw fa-chevron-down"></i> <?php print "PHP Version: " . PHP_VERSION ?></p>

          <?php $mysqlError = check_mysql_available(); ?>

          <?php if(is_null($mysqlError)): ?>
            <p class="lead success"><i class="fa fa-fw fa-chevron-down"></i> MySQL Server is available and database exists.</p>

          <?php else: ?>
            <p class="lead fail"><i class="fa fa-fw fa-times"></i> MySQL Server is NOT available. Error: <?php print $mysqlError; ?></p>
          <?php endif; ?>

          <?php $redisError = check_redis_available(); ?>

          <?php if(is_null($redisError)): ?>
            <p class="lead success"><i class="fa fa-fw fa-chevron-down"></i> Redis Server is available.</p>

          <?php else: ?>
            <p class="lead fail"><i class="fa fa-fw fa-times"></i> Redis Server is NOT available. Error: <?php print $redisError; ?></p>
          <?php endif; ?>

      </div>
    </div>
  </div>
</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/jquery/dist/jquery.min.map"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>