<?php

error_reporting(0);

function check_mysql_available() {
  $credentials = [
    'host' => 'localhost',
    'port' => 3306,
    'socket' => '',
    'username' => 'root',
    'password' => 'root',
    'database' => 'scotchbox',
  ];

  try {
    $handle = mysqli_connect($credentials['host'], $credentials['username'], $credentials['password'], $credentials['database'], $credentials['port'], $credentials['socket']);

    if(empty($handle) || !$handle->ping()) {
      $error = error_get_last();
      throw new Exception($error['message']);
    }
  }
  catch(Exception $e) {
    return $e->getMessage();
  }

  return NULL;
}

function check_redis_available() {
  $credentials = [
    'host' => 'localhost',
    'port' => 6379,
  ];

  try {
    $handle = new Redis();
    $handle->connect($credentials['host'], $credentials['port']);

    $handle->ping();
  }
  catch(Exception $e) {
    return $e->getMessage();
  }

  return NULL;
}